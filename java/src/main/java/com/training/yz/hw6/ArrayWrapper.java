package com.training.yz.hw6;

public class ArrayWrapper<T> {
    private T[] array;

    public ArrayWrapper(T[] array) {
        this.array = array;
    }

    public T get(int index) {
        index--;
        checkIndex(index);
        return array[index];
    }

    public boolean replace(int index, T value) {
        index--;
        checkIndex(index);

        if (value instanceof String) {
            if (((String) value).length() > ((String) array[index]).length()) {
                array[index] = value;
                return true;
            } else {
                return false;
            }
        }

        if (value instanceof Integer) {
            if ((Integer) value > (Integer) array[index]) {
                array[index] = value;
                return true;
            } else {
                return false;
            }
        }

        array[index] = value;
        return true;
    }

    private void checkIndex(int index) {
        if (index < 0 || index > array.length - 1) {
            throw new IncorrectArrayWrapperIndex();
        }
    }
}
