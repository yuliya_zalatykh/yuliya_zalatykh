package com.training.yz.hw7.task1;

import java.util.Arrays;
import java.util.Comparator;

public class SortingBySumOfDigits implements Comparator<Integer> {

    public int calcSumOfDigits(int number) {
        int sum = 0;
        number = Math.abs(number);
        while (number != 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

    @Override
    public int compare(Integer first, Integer second) {
        return calcSumOfDigits(first) > calcSumOfDigits(second) ? 1 : -1;
    }

    public static void main(String[] args) {
        Integer[] array = new Integer[]{12, -17, 42, 234, 3, 45};

        Arrays.sort(array, new SortingBySumOfDigits());

        for (int element : array) {
            System.out.print(element + " ");
        }
    }
}
