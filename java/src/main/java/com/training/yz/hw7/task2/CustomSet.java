package com.training.yz.hw7.task2;

import java.util.HashSet;
import java.util.Set;

public class CustomSet<T> extends HashSet<T> {
    public CustomSet() {
        super();
    }

    public CustomSet(CustomSet<T> set) {
        super(set);
    }

    public void findUnion(Set<T> set) {
        this.addAll(set);
    }

    public void findIntersection(Set<T> set) {
        this.retainAll(set);
    }

    public void findDifference(Set<T> set) {
        this.removeAll(set);
    }

    public void findSymmetricDifference(Set<T> set) {
        CustomSet<T> temp = new CustomSet<>(this);
        findUnion(set);
        temp.findIntersection(set);
        findDifference(temp);
    }
}
