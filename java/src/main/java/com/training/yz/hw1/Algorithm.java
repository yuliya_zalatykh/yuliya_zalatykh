package com.training.yz.hw1;

import java.util.Scanner;

public class Algorithm {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter three integers");

        int algorithmId = scanner.nextInt();
        int loopType = scanner.nextInt();
        int n = scanner.nextInt();

        switch (algorithmId) {
            case 1:
                calcFibo(loopType, n);
                break;
            case 2:
                calcFact(loopType, n);
                break;
            default:
                System.out.println("Invalid algorithm id");
        }
    }

    private static void calcFibo(int loopType, int n) {
        switch (loopType) {
            case 1:
                calcFiboUsingWhile(n);
                break;
            case 2:
                calcFiboUsingDoWhile(n);
                break;
            case 3:
                calcFiboUsingFor(n);
                break;
            default:
                System.out.println("Invalid type of loop");
        }
    }

    private static void calcFact(int loopType, int n) {
        switch (loopType) {
            case 1:
                calcFactUsingWhile(n);
                break;
            case 2:
                calcFactUsingDoWhile(n);
                break;
            case 3:
                calcFactUsingFor(n);
                break;
            default:
                System.out.println("Invalid type of loop");
        }
    }

    private static void calcFiboUsingWhile(int n) {
        int i = 0;
        int[] f = new int[n];

        while (i < n) {
            f[i] = (i < 2) ? 1 : f[i - 2] + f[i - 1];
            System.out.printf("%d ", f[i]);
            ++i;
        }
    }

    private static void calcFiboUsingDoWhile(int n) {
        int i = 0;
        int[] f = new int[n];

        do {
            f[i] = (i < 2) ? 1 : f[i - 2] + f[i - 1];
            System.out.printf("%d ", f[i]);
            ++i;
        }
        while (i < n);
    }

    private static void calcFiboUsingFor(int n) {
        int[] f = new int[n];

        for (int i = 0; i < n; i++) {
            f[i] = (i < 2) ? 1 : f[i - 2] + f[i - 1];
            System.out.printf("%d ", f[i]);
        }
    }

    private static void calcFactUsingWhile(int n) {
        long result = 1;
        int i = 1;

        while (i <= n) {
            result = result * i;
            i++;
        }
        System.out.println(result);
    }

    private static void calcFactUsingDoWhile(int n) {
        long result = 1;
        int i = 1;

        do {
            result *= i;
            i++;
        }
        while (i <= n);
        System.out.println(result);
    }

    private static void calcFactUsingFor(int n) {
        long result = 1;

        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        System.out.println(result);
    }
}
