package com.training.yz.hw4.task2;

import java.util.Scanner;

public class Sorting {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter array length");
        int arrSize = scanner.nextInt();
        int[] array = new int[arrSize];

        System.out.println("Fill array");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Select sorting type of array");
        System.out.println("1: Bubble sort; other: Selection sort");

        int sortType = scanner.nextInt();

        Sorter sorter = sortType == 1 ? new BubbleSort() : new SelectionSort();
        SortingContext sortingContext = new SortingContext(sorter);
        sortingContext.executeSortStrategy(array);
        printArray(array);
    }

    public static void printArray(int[] array) {
        System.out.println("Sorted array");
        for (int element : array) {
            System.out.printf("%s ", element);
        }
    }
}
