package com.training.yz.hw4.task2;

public interface Sorter {
    public void sort(int[] array);
}
