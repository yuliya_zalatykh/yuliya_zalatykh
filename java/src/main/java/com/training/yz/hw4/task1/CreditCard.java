package com.training.yz.hw4.task1;

public class CreditCard extends Card {

    public CreditCard(String cardHolder, double balance) {
        super(cardHolder, balance);
    }

    public CreditCard(String cardHolder) {
        super(cardHolder);
    }
}
