package com.training.yz.hw4.task1;

public class CardOperationException extends RuntimeException {

    public CardOperationException(String message) {
        super(message);
    }
}
