package com.training.yz.hw4.task1;

public class Atm {

    private Card card;

    public double getBalance() {
        return card.getBalance();
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public void ejectCard() {
        card = null;
    }

    public void topUp(double amount) {
        if (!card.increase(amount)) {
            throw new CardOperationException("Invalid top up amount");
        }
    }

    public void withdraw(double amount) {
        if (!card.decrease(amount)) {
            throw new CardOperationException("Invalid withdraw amount");
        }
    }
}
