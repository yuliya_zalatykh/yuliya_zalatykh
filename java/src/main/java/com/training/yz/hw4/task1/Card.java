package com.training.yz.hw4.task1;

public class Card {

    private String cardHolder;
    protected double balance;

    public Card(String cardHolder, double balance) {
        this.cardHolder = cardHolder;
        this.balance = balance;
    }

    public Card(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public double getBalance() {
        return balance;
    }

    public boolean increase(double amount) {
        boolean isAmountValid = checkAmount(amount);
        if (isAmountValid) {
            balance += amount;
            return true;
        } else {
            return false;
        }
    }

    public boolean decrease(double amount) {
        if (checkAmount(amount)) {
            balance -= amount;
            return true;
        } else {
            return false;
        }
    }

    protected boolean checkAmount(double amount) {
        return amount > 0;
    }

    public String convert(String currency, double conversionRate) {
        if (conversionRate > 0) {
            double result = balance / conversionRate;
            return String.format("%s: %.2f", currency, result);
        } else {
            return "Conversion fail";
        }
    }
}
