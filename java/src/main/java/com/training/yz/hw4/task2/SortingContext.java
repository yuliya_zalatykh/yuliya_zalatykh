package com.training.yz.hw4.task2;

public class SortingContext {

    private Sorter sortStrategy;

    public SortingContext(Sorter sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public void executeSortStrategy(int [] array){
        sortStrategy.sort(array);
    }
}
