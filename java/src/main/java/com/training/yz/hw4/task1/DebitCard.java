package com.training.yz.hw4.task1;

public class DebitCard extends Card {

    public DebitCard(String cardHolder, double balance) {
        super(cardHolder, balance);
    }

    public DebitCard(String cardHolder) {
        super(cardHolder);
    }

    @Override
    public boolean decrease(double amount) {
        if (balance >= amount) {
            return super.decrease(amount);
        } else {
            return false;
        }
    }
}
