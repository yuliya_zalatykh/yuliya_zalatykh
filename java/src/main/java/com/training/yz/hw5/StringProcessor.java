package com.training.yz.hw5;

import java.util.Arrays;
import java.util.Scanner;

public class StringProcessor {
    public static void main(String[] args) {
        System.out.println("Type any text in latin");
        String text = new Scanner(System.in).nextLine();
        printWordsInAlphabeticalOrder(text);
    }

    public static void printWordsInAlphabeticalOrder(String text) {
        String[] normalizedWords = text.replaceAll("[^a-zA-Z\\s]", " ")
                .toLowerCase()
                .trim()
                .split("\\s+");

        Arrays.sort(normalizedWords);

        String[] sortedWords = String.join(" ", normalizedWords)
                .replaceAll("\\b(\\w+)(?:\\s+\\1\\b)+", "$1")
                .split("\\s");

        for (int current = 0; current < sortedWords.length; current++) {
            char firstChar = sortedWords[current].charAt(0);
            System.out.printf("%c: %s ", Character.toUpperCase(firstChar), sortedWords[current]);
            for (int next = current + 1; next < sortedWords.length; next++) {
                if (firstChar == sortedWords[next].charAt(0)) {
                    System.out.printf("%s ", sortedWords[next]);
                    current++;
                } else {
                    System.out.println();
                    break;
                }
            }
        }
    }
}

