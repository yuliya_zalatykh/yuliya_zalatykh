package com.training.yz.hw2.task1;

public class Main {
    public static void main(String[] args) {
        System.out.println("Default values for primitives:");
        System.out.println("int: " + Primitives.getVarInt());
        System.out.println("long: " + Primitives.getVarLong());
        System.out.println("short: " + Primitives.getVarShort());
        System.out.println("byte: " + Primitives.getVarByte());
        System.out.println("double: " + Primitives.getVarDouble());
        System.out.println("float: " + Primitives.getVarFloat());
        System.out.println("char: " + Primitives.getVarChar());
        System.out.println("boolean: " + Primitives.getVarBoolean());
        System.out.println();
        System.out.println("Default values for references:");
        System.out.println("String: " + References.getVarString());
        System.out.println("Array: " + References.getVarArray());
    }
}
