package com.training.yz.hw2.task1;

public class Primitives {
    private static int varInt;
    private static long varLong;
    private static short varShort;
    private static byte varByte;
    private static double varDouble;
    private static float varFloat;
    private static char varChar;
    private static boolean varBoolean;

    public static int getVarInt() {
        return varInt;
    }

    public static long getVarLong() {
        return varLong;
    }

    public static short getVarShort() {
        return varShort;
    }

    public static byte getVarByte() {
        return varByte;
    }

    public static double getVarDouble() {
        return varDouble;
    }

    public static float getVarFloat() {
        return varFloat;
    }

    public static char getVarChar() {
        return varChar;
    }

    public static boolean getVarBoolean() {
        return varBoolean;
    }
}
