package com.training.yz.hw2.task2;

public class Sequence {
    private static String staticVar = print("Static variable");
    private String regularVar = print("Regular variable");

    {
        System.out.println("First initialization block");
    }

    {
        System.out.println("Second initialization block");
    }

    static {
        System.out.println("First static block");
    }

    static {
        System.out.println("Second static block");
    }

    public Sequence() {
        System.out.println("Constructor");
    }

    public static String print(String message) {
        System.out.println(message);
        return message;
    }

    public static void main(String[] args) {
        Sequence sequence = new Sequence();
    }
}
