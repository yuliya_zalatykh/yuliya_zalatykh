package com.training.yz.hw2.task3;

import java.util.ArrayList;
import java.util.List;

public class MaxCorrectFibo {
    public static void main(String[] args) {
        int maxIntOrder = findMaxValueOrder(Integer.MAX_VALUE);
        int maxLongOrder = findMaxValueOrder(Long.MAX_VALUE);

        System.out.printf("Maximum for Integer is %d\n Maximum for Long is %d", maxIntOrder, maxLongOrder);
    }

    private static final List<Long> ARRAY_LIST = new ArrayList<>();

    public static long calcFibo(int item) {
        if (item < ARRAY_LIST.size()) {
            return ARRAY_LIST.get(item);
        }
        if (item == 0) {
            ARRAY_LIST.add(0L);
            return 0;
        }
        if (item == 1) {
            ARRAY_LIST.add(1L);
            return 1;
        }
        long count = calcFibo(item - 2) + calcFibo(item - 1);
        ARRAY_LIST.add(count);
        return count;
    }

    public static int findMaxValueOrder(long limit) {
        int order = 0;
        long value = calcFibo(order);

        while (value >= 0 && value <= limit) {
            value = calcFibo(++order);
        }
        return --order;
    }
}
