package com.training.yz.hw8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class EmployeeController {
    private static final String FILE_PATH = "src/main/java/com/training/yz/hw8/employees.txt";
    private static ArrayList<Employee> employees = new ArrayList<>();

    public static void addEmployee(Employee employee) {
        String message = employees.add(employee) ? "The employee was added" : "The employee wasn't added";
        System.out.println(message);
    }

    public static void loadEmployees() {
        File file = new File(FILE_PATH);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        ArrayList<Employee> employeesFromFile = new ArrayList<>();
        try (FileInputStream inputStream = new FileInputStream(FILE_PATH);
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
            Employee employee;
            while (inputStream.available() > 0) {
                employee = (Employee) objectInputStream.readObject();
                employeesFromFile.add(employee);
            }
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
        employees = employeesFromFile;
    }

    public static void deleteEmployeeById(long id) {
        String message = employees.removeIf(employee -> employee.getId() == id) ?
                "The employee was successfully removed" :
                "The employee with current id wasn't found";
        System.out.println(message);
    }

    public static void save() {
        try (FileOutputStream outputStream = new FileOutputStream(FILE_PATH);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            for (Employee employee : employees) {
                objectOutputStream.writeObject(employee);
            }
            objectOutputStream.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static void showAll() {
        employees.forEach(System.out::println);
    }
}
