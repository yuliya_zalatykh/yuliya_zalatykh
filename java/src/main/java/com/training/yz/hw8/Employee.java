package com.training.yz.hw8;

import java.io.Serializable;

public class Employee implements Serializable {
    private long id;
    private String firstName;
    private String lastName;

    public Employee(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + firstName + " " + lastName;
    }
}
