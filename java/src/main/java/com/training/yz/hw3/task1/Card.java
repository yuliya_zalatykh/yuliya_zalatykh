package com.training.yz.hw3.task1;

public class Card {

    private String cardHolder;
    private double balance;

    public Card(String cardHolder, double balance) throws RuntimeException {
        if (balance < 0) {
            throw new RuntimeException("Invalid balance");
        }
        this.balance = balance;
        this.cardHolder = cardHolder;
    }

    public Card(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public double getBalance() {
        return balance;
    }

    public void increase(double amount) {
        if (amount > 0) {
            balance += amount;
        }
    }

    public void withdraw(double amount) {
        if (amount <= balance) {
            balance -= amount;
        }
    }

    public String convert(String currency, double conversionRate) {
        if (conversionRate > 0) {
            double result = balance / conversionRate;
            return String.format("%s: %.2f", currency, result);
        } else {
            return "Conversion fail";
        }
    }

}
