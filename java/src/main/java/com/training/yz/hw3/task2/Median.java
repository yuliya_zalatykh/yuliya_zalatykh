package com.training.yz.hw3.task2;

import java.util.Arrays;

public class Median {

    public static float median(int[] arr) {
        Arrays.sort(arr);
        float median;

        if (arr.length % 2 == 0) {
            median = (arr[arr.length / 2 - 1] + arr[arr.length / 2]) / 2f;
        } else {
            median = arr[arr.length / 2];
        }
        return median;
    }

    public static double median(double[] arr) {
        Arrays.sort(arr);
        double median;

        if (arr.length % 2 == 0) {
            median = (arr[arr.length / 2 - 1] + arr[arr.length / 2]) / 2d;
        } else {
            median = arr[arr.length / 2];
        }
        return median;
    }

}
