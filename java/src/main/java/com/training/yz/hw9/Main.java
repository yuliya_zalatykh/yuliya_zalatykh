package com.training.yz.hw9;

import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            EmployeeController employeeController = EmployeeController.getInstance();
            boolean isActive = true;
            while (isActive) {
                System.out.println("Choose operation: [1] Add [2] Delete [3] Show all [4] Exit");
                String userChoice = readInput("[1-4]");
                switch (userChoice) {
                    case "1":
                        long id = System.currentTimeMillis();
                        System.out.print("Enter first name: ");
                        String firstName = scanner.next();
                        System.out.print("Enter last name: ");
                        String lastName = scanner.next();
                        Employee newEmployee = new Employee(id, firstName, lastName);
                        employeeController.addEmployee(newEmployee);
                        break;
                    case "2":
                        System.out.print("Enter id: ");
                        long deleteId = Long.parseLong(readInput("\\d+"));
                        employeeController.deleteEmployeeById(deleteId);
                        break;
                    case "3":
                        employeeController.getEmployees().forEach(System.out::println);
                        break;
                    case "4":
                        isActive = false;
                        break;
                }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    private static String readInput(String regExp) {
        String temp;
        boolean isMatch;
        do {
            temp = new Scanner(System.in).next();
            isMatch = Pattern.matches(regExp, temp);
            if (!isMatch) {
                System.out.println("Invalid option. Please, try again");
            }
        } while (!isMatch);
        return temp;
    }
}
