package com.training.yz.hw9;

import org.sqlite.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class EmployeeController {
    private static EmployeeController instance = null;
    private static final String DB_PATH = "jdbc:sqlite:src/main/resources/db/employees.db";

    public static synchronized EmployeeController getInstance() throws SQLException {
        if (instance == null)
            instance = new EmployeeController();
        return instance;
    }

    private EmployeeController() throws SQLException {
        DriverManager.registerDriver(new JDBC());
    }

    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_PATH);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT id, firstname, lastname FROM Employees")) {

            while (resultSet.next()) {
                employees.add(new Employee(resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname")));
            }

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return employees;
    }

    public void addEmployee(Employee employee) {
        try (Connection connection = DriverManager.getConnection(DB_PATH);
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO Employees(`id`, `firstname`, `lastname`) " + "VALUES(?, ?, ?)")) {
            statement.setObject(1, employee.getId());
            statement.setObject(2, employee.getFirstName());
            statement.setObject(3, employee.getLastName());
            statement.execute();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void deleteEmployeeById(long id) {
        try (Connection connection = DriverManager.getConnection(DB_PATH);
             PreparedStatement statement = connection.prepareStatement(
                     "DELETE FROM Employees WHERE id = ?")) {
            statement.setObject(1, id);
            statement.execute();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
