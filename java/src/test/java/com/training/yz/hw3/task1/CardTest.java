package com.training.yz.hw3.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CardTest {

    private Card card;

    @Before
    public void setUpCard() {
        card = new Card("Yuliya Zalatykh", 2000);
    }

    @Test
    public void testIncrease() {
        card.increase(500.99);
        Assert.assertEquals(2500.99, card.getBalance(), 0);
    }

    @Test
    public void testIncreaseNegative() {
        card.increase(-500.99);
        Assert.assertEquals(2000, card.getBalance(), 0);
    }

    @Test
    public void testWithdraw() {
        card.withdraw(1500);
        Assert.assertEquals(500, card.getBalance(), 0);
    }

    @Test
    public void testWithdrawNegative() {
        card.withdraw(2500);
        Assert.assertEquals(2000, card.getBalance(), 0);
    }

    @Test
    public void testConvert() {
        String result = card.convert("USD", 2.62);
        Assert.assertEquals("USD: 763,36", result);
    }

    @Test
    public void testConvertNegative() {
        String result = card.convert("USD", 0);
        Assert.assertEquals("Conversion fail", result);
    }

}
