package com.training.yz.hw4.task1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CreditCardTest {

    private CreditCard creditCard;

    @Before
    public void setUp() {
        creditCard = new CreditCard("Olivia Smith", 900);
    }

    @Test
    public void testTopUp() {
        boolean operationResult = creditCard.increase(100);
        assertEquals(true, operationResult);
        assertEquals(1000, creditCard.getBalance(), 0);
    }

    @Test
    public void testTopUpNegative() {
        boolean operationResult = creditCard.increase(-400);
        assertEquals(false, operationResult);
        assertEquals(900, creditCard.getBalance(), 0);
    }

    @Test
    public void testWithdraw() {
        boolean operationResult = creditCard.decrease(900);
        assertEquals(true, operationResult);
        assertEquals(0, creditCard.getBalance(), 0);
    }

    @Test
    public void testWithdrawNegative() {
        boolean operationResult = creditCard.decrease(-1);
        assertEquals(false, operationResult);
        assertEquals(900, creditCard.getBalance(), 0);
    }

    @Test
    public void testWithdrawMoreThanBalance() {
        boolean operationResult = creditCard.decrease(910);
        assertEquals(true, operationResult);
        assertEquals(-10, creditCard.getBalance(), 0.001);
    }
}
