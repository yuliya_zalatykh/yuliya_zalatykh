package com.training.yz.hw4.task1;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class DebitCardTest {

    private DebitCard debitCard;

    @Before
    public void setUp() {
        debitCard = new DebitCard("John Snow", 5000);
    }

    @Test
    public void testTopUp() {
        boolean operationResult = debitCard.increase(500);
        assertEquals(true, operationResult);
        assertEquals(5500, debitCard.getBalance(), 0);
    }

    @Test
    public void testTopUpNegative() {
        boolean operationResult = debitCard.increase(-100);
        assertEquals(false, operationResult);
        assertEquals(5000, debitCard.getBalance(), 0);
    }

    @Test
    public void testWithdraw() {
        boolean operationResult = debitCard.decrease(400);
        assertEquals(true, operationResult);
        assertEquals(4600, debitCard.getBalance(), 0);
    }

    @Test
    public void testWithdrawNegative() {
        boolean operationResult = debitCard.decrease(-1);
        assertEquals(false, operationResult);
        assertEquals(5000, debitCard.getBalance(), 0);
    }

    @Test
    public void testWithdrawMoreThanBalance() {
        boolean operationResult = debitCard.decrease(5001);
        assertEquals(false, operationResult);
        assertEquals(5000, debitCard.getBalance(), 0);
    }
}
