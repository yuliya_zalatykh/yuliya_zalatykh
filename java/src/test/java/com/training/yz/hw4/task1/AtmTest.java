package com.training.yz.hw4.task1;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class AtmTest {

    private static Atm atm;
    private DebitCard debitCard;
    private CreditCard creditCard;

    @BeforeClass
    public static void setUp() {
        atm = new Atm();
    }

    @Before
    public void prepare() {
        creditCard = new CreditCard("Tomas Edison", 1000);
        debitCard = new DebitCard("Nikola Tesla", 450);
    }

    @After
    public void clear() {
        atm.ejectCard();
    }

    @Test
    public void testTopUpCreditCard() {
        atm.setCard(creditCard);
        atm.topUp(1000);
        assertEquals(2000, atm.getBalance(), 0);
    }

    @Test
    public void testTopUpDebitCard() {
        atm.setCard(debitCard);
        atm.topUp(50);
        assertEquals(500, atm.getBalance(), 0);
    }

    @Test(expected = CardOperationException.class)
    public void testTopUpNegativeCreditCard() {
        atm.setCard(creditCard);
        atm.topUp(-10);
    }

    @Test(expected = CardOperationException.class)
    public void testTopUpNegativeDebitCard() {
        atm.setCard(debitCard);
        atm.topUp(-20);
    }

    @Test
    public void testWithdrawCreditCard() {
        atm.setCard(creditCard);
        atm.withdraw(1500);
        assertEquals(-500, atm.getBalance(), 0);
    }

    @Test
    public void testWithdrawDebitCard() {
        atm.setCard(debitCard);
        atm.withdraw(50);
        assertEquals(400, atm.getBalance(), 0);
    }

    @Test(expected = CardOperationException.class)
    public void testWithdrawNegativeCreditCard() {
        atm.setCard(creditCard);
        atm.withdraw(-1260);
    }

    @Test(expected = CardOperationException.class)
    public void testWithdrawNegativeDebitCard() {
        atm.setCard(creditCard);
        atm.withdraw(-10);
    }

    @Test(expected = CardOperationException.class)
    public void testWithdrawMoreThanBalanceDebitCard() {
        atm.setCard(debitCard);
        atm.withdraw(500);
    }
}
