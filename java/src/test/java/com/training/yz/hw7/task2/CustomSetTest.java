package com.training.yz.hw7.task2;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CustomSetTest {
    private CustomSet<Integer> firstSet;
    private static CustomSet<Integer> secondSet;
    private CustomSet<Integer> expected;

    @BeforeClass
    public static void setUp() {
        secondSet = new CustomSet<>();
        secondSet.add(1);
        secondSet.add(2);
        secondSet.add(5);
        secondSet.add(6);
    }

    @Before
    public void init() {
        firstSet = new CustomSet<>();
        firstSet.add(1);
        firstSet.add(2);
        firstSet.add(3);
        firstSet.add(4);

        expected = new CustomSet<>();
    }

    @Test
    public void testFindUnion() {
        firstSet.findUnion(secondSet);
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        expected.add(6);
        assertEquals(expected, firstSet);
    }

    @Test
    public void testFindIntersection() {
        firstSet.findIntersection(secondSet);
        expected.add(1);
        expected.add(2);
        assertEquals(expected, firstSet);
    }

    @Test
    public void testFindDifference() {
        firstSet.findDifference(secondSet);
        expected.add(3);
        expected.add(4);
        assertEquals(expected, firstSet);
    }

    @Test
    public void testFindSymmetricDifference() {
        firstSet.findSymmetricDifference(secondSet);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        expected.add(6);
        assertEquals(expected, firstSet);
    }
}
