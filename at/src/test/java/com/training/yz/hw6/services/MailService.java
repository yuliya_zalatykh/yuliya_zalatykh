package com.training.yz.hw6.services;

import com.training.yz.hw6.bussinesObjects.Letter;
import com.training.yz.hw6.pageObjects.DraftsPage;
import com.training.yz.hw6.pageObjects.MainPage;
import com.training.yz.hw6.pageObjects.SentPage;
import com.training.yz.hw6.pageObjects.WriteLetterPage;
import org.openqa.selenium.WebDriver;

public class MailService {
    private WebDriver driver;
    private MainPage mainPage;
    private WriteLetterPage writeLetterPage;

    public MailService(WebDriver driver) {
        this.driver = driver;
    }

    public String getUserLogin() {
        return new MainPage(driver).getUserLogin();
    }

    public void writeLetter(Letter letter) {
        mainPage = new MainPage(driver);
        mainPage.clickWriteLetterButton();

        writeLetterPage = new WriteLetterPage(driver);
        writeLetterPage.typeAddress(letter.getAddress());
        writeLetterPage.typeSubject(letter.getSubject());
        writeLetterPage.typeMessage(letter.getMessage());
        writeLetterPage.clickSaveButton();
        writeLetterPage.clickCloseButton();
    }

    public void openDraftLetter() {
        mainPage = new MainPage(driver);
        mainPage.openDraftsFolder();
        DraftsPage draftsPage = new DraftsPage(driver);
        draftsPage.openDraftLetter();
    }

    public Letter getLetter() {
        writeLetterPage = new WriteLetterPage(driver);
        return new Letter(
                writeLetterPage.getAddress(),
                writeLetterPage.getSubject(),
                writeLetterPage.getMessage()
        );
    }

    public void closeViewLetter() {
        new WriteLetterPage(driver).clickCloseButton();
    }

    public void sendLetter() {
        writeLetterPage = new WriteLetterPage(driver);
        writeLetterPage.sendLetter();
        writeLetterPage.waitForSendButtonInvisibility();
        writeLetterPage.refreshPage();
    }

    public String getFirstDraftLetterInfo() {
        return new DraftsPage(driver).getFirstLetterText();
    }

    public void openSentFolder() {
        new MainPage(driver).openSentFolder();
    }

    public String getFirstSentLetterInfo() {
        return new SentPage(driver).getSentLetterText();
    }
}
