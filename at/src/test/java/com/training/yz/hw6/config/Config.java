package com.training.yz.hw6.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
    private static final String PATH = "src/test/java/com/training/yz/hw6/config/config.properties";

    public static String get(String propertyName) {
        String property = null;
        Properties properties = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(PATH)) {
            properties.load(fileInputStream);
            property = properties.getProperty(propertyName);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return property;
    }
}
