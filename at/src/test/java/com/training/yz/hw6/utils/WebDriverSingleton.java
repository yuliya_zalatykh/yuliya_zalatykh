package com.training.yz.hw6.utils;

import com.training.yz.hw6.config.Config;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverSingleton {
    private static WebDriver driver;

    private WebDriverSingleton() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            String browser = Config.get("browser");
            switch (browser) {
                case "chrome":
                    WebDriverManager.getInstance(ChromeDriver.class).setup();
                    driver = new ChromeDriver();
                    break;
                case "firefox":
                    WebDriverManager.getInstance(FirefoxDriver.class).setup();
                    driver = new FirefoxDriver();
                    break;
                default:
                    WebDriverManager.getInstance(ChromeDriver.class).setup();
                    driver = new ChromeDriver();
            }
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        return driver;
    }
}
