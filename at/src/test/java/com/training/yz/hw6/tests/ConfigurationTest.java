package com.training.yz.hw6.tests;

import com.training.yz.hw6.bussinesObjects.User;
import com.training.yz.hw6.utils.WebDriverSingleton;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

public class ConfigurationTest {
    protected static WebDriver driver;
    protected User user = new User("test_account_at@mail.ru", "superSecurePassword");

    @BeforeClass
    public void setUp() {
        driver = WebDriverSingleton.getInstance();
    }

    @AfterMethod(alwaysRun = true)
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass(alwaysRun = true)
    public void reset() {
        driver.quit();
        driver = null;
    }
}
