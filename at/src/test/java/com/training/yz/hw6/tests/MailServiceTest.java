package com.training.yz.hw6.tests;

import com.training.yz.hw6.bussinesObjects.Letter;
import com.training.yz.hw6.services.LoginService;
import com.training.yz.hw6.services.MailService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class MailServiceTest extends ConfigurationTest {
    private static final String MESSAGE = "Hello my friend";
    private static final String URL = "https://mail.ru";

    @BeforeMethod
    public void login() {
        driver.get(URL);
        new LoginService(driver).signIn(user);
    }

    @Test
    public void testLogin() {
        Assert.assertEquals(new MailService(driver).getUserLogin(), user.getLogin());
    }

    @Test
    public void testSaveAsDraft() {
        MailService mailService = new MailService(driver);
        Letter letter = new Letter(user.getLogin(), String.valueOf(System.currentTimeMillis()), MESSAGE);

        mailService.writeLetter(letter);
        mailService.openDraftLetter();
        Assert.assertEquals(mailService.getLetter().toString(), letter.toString());
        mailService.closeViewLetter();
    }

    @Test
    public void testSendLetter() {
        SoftAssert softAssert = new SoftAssert();
        MailService mailService = new MailService(driver);
        Letter letter = new Letter(user.getLogin(), String.valueOf(System.currentTimeMillis()), MESSAGE);

        mailService.writeLetter(letter);
        mailService.openDraftLetter();
        mailService.sendLetter();
        softAssert.assertFalse(mailService.getFirstDraftLetterInfo().contains(letter.getSubject()));
        mailService.openSentFolder();
        softAssert.assertTrue(mailService.getFirstSentLetterInfo().contains(letter.getSubject()));
        softAssert.assertAll();
    }

    @Test
    public void testLogOut() {
        LoginService loginService = new LoginService(driver);
        loginService.logOut();
        Assert.assertTrue(loginService.isLoggedOut());
    }
}
