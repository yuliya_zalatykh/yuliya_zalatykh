package com.training.yz.hw6.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WriteLetterPage extends AbstractPage {
    private final By addressInput = By.cssSelector("[data-type='to'] input");
    private final By subjectInput = By.xpath("//input[@name='Subject']");
    private final By messageInput = By.cssSelector("[role='textbox']");
    private final By saveButton = By.cssSelector("[data-title-shortcut='Ctrl+S']");
    private final By closeButton = By.xpath("//*[@data-promo-id='extend']/following::button");
    private final By contactItem = By.cssSelector("[data-type='to'] [title]");
    private final By messageInputDraft = By.cssSelector("[role='textbox'] [id]>[class]>div:nth-child(1)");
    private final By sendButton = By.cssSelector("[data-title-shortcut='Ctrl+Enter']");

    public WriteLetterPage(WebDriver driver) {
        super(driver);
    }

    public void typeAddress(String address) {
        driver.findElement(addressInput).sendKeys(address);
    }

    public void typeSubject(String subject) {
        driver.findElement(subjectInput).sendKeys(subject);
    }

    public void typeMessage(String message) {
        driver.findElement(messageInput).sendKeys(message);
    }

    public void clickSaveButton() {
        driver.findElement(saveButton).click();
    }

    public void clickCloseButton() {
        driver.findElement(closeButton).click();
    }

    public String getAddress() {
        return driver.findElement(contactItem).getText();
    }

    public String getSubject() {
        return driver.findElement(subjectInput).getAttribute("value");
    }

    public String getMessage() {
        return driver.findElement(messageInputDraft).getText();
    }

    public void sendLetter() {
        driver.findElement(sendButton).click();
    }

    public void waitForSendButtonInvisibility() {
        wait.elementToBeInvisible(sendButton);
    }
}
