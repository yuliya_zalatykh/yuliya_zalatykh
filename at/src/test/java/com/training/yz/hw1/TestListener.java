package com.training.yz.hw1;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.printf("[START] - %s\n", method.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.printf("[FINISH] - %s » %s\n",
                method.getTestMethod().getMethodName(), testResult.getStatus() == 1 ? "Success" : "Fail");
    }
}
