package com.training.yz.hw1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TrigonometricOperationsTest extends BaseConfigurationTest {

    @Test(dataProvider = "sinProvider")
    public void testSin(double value, double expected) {
        double result = calculator.sin(Math.toRadians(value));
        Assert.assertEquals(result, expected);
    }

    @DataProvider
    public static Object[][] sinProvider() {
        return new Object[][]{
                {0, 0},
                {30, 0.5},
                {45, 0.7071067811865475},
                {60, 0.8660254037844386},
                {90, 1},
                {180, 0},
                {270, -1},
                {360, 0},
                {-45, -0.7071067811865475},
                {400, 0.6427876096865391}
        };
    }

    @Test(dataProvider = "cosProvider", priority = 1)
    public void testCos(double value, double expected) {
        double result = calculator.cos(Math.toRadians(value));
        Assert.assertEquals(result, expected);
    }

    @DataProvider
    public static Object[][] cosProvider() {
        return new Object[][]{
                {0, 1},
                {30, 0.8660254037844387},
                {45, 0.7071067811865475},
                {60, 0.5},
                {90, 0},
                {180, -1},
                {270, 0},
                {360, 1},
                {-45, -0.7071067811865475},
                {400, 0.7660444431189781}
        };
    }

    @Test(dataProvider = "tgProvider", priority = 2)
    public void testTg(double value, double expected) {
        double result = calculator.tg(Math.toRadians(value));
        Assert.assertEquals(result, expected);
    }

    @DataProvider
    public static Object[][] tgProvider() {
        return new Object[][]{
                {0, 0},
                {30, 0.5773502691896257},
                {45, 1},
                {60, 1.7320508075688767},
                {180, 0},
                {360, 0},
                {-45, -1},
                {400, 0.8390996311772797}
        };
    }

    @Test (expectedExceptions = Exception.class, priority = 3)
    public void testTgNotExisting(){
        calculator.tg(Math.toRadians(90));
    }

    @Test(dataProvider = "ctgProvider", priority = 4)
    public void testCtg(double value, double expected) {
        double result = calculator.ctg(Math.toRadians(value));
        Assert.assertEquals(result, expected);
    }

    @DataProvider
    public static Object[][] ctgProvider() {
        return new Object[][]{
                {30, 1.7320508075688774},
                {45, 1},
                {60, 0.577350269189626},
                {90, 0},
                {270, 0},
                {-45, -1},
                {400, 1.1917535925942104}
        };
    }

    @Test(expectedExceptions = Exception.class, priority = 5)
    public void testCtgNoExisting() {
        calculator.ctg(360);
    }
}
