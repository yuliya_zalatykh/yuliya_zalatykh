package com.training.yz.hw1;

import com.epam.tat.module4.Calculator;
import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseConfigurationTest {

    protected static Calculator calculator;

    @BeforeClass
    public static void setUp() {
        calculator = new Calculator();
    }

    @AfterClass
    public static void tearDown() {
        calculator = null;
    }
}
