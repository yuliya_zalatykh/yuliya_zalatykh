package com.training.yz.hw1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class OtherOperationsTest extends BaseConfigurationTest {

    private long value;

    @Factory(dataProvider = "valueProvider")
    public OtherOperationsTest(long value) {
        this.value = value;
    }

    @DataProvider
    public static Object[][] valueProvider() {
        return new Object[][]{
                {1},
                {0},
                {-1}
        };
    }

    @Test
    public void testIsPositive() {
        boolean result = calculator.isPositive(value);
        if (value > 0) {
            Assert.assertTrue(result);
        } else {
            Assert.assertFalse(result);
        }
    }

    @Test
    public void testIsNegative() {
        boolean result = calculator.isNegative(value);
        if (value < 0) {
            Assert.assertTrue(result);
        } else {
            Assert.assertFalse(result);
        }
    }
}
