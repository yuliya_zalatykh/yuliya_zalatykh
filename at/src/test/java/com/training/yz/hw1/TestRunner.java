package com.training.yz.hw1;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        List<String> files = Arrays.asList(
                "./src/test/resources/arithmetic-operations.xml",
                "./src/test/resources/trigonometric-operations.xml",
                "./src/test/resources/other-operations.xml");
        testNG.setTestSuites(files);

        System.out.println("Enter 'yes' to run tests in parallel");
        String userInput = new Scanner(System.in).next();
        if (userInput.equals("yes")) {
            testNG.setParallel(XmlSuite.ParallelMode.CLASSES);
        }
        testNG.run();
    }
}
