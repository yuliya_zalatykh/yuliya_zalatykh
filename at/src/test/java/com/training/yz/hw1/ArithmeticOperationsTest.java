package com.training.yz.hw1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ArithmeticOperationsTest extends BaseConfigurationTest {

    @Test(dataProvider = "sumLongProvider")
    public void testSumLong(long first, long second, long expected) {
        long result = calculator.sum(first, second);
        Assert.assertEquals(result, expected);
    }

    @DataProvider
    public Object[][] sumLongProvider() {
        return new Object[][]{
                {2, 3, 5},
                {0, 0, 0},
                {-70, 20, -50},
                {100000, 200000, 300000}
        };
    }

    @Test
    public void testSumDouble() {
        double result = calculator.sum(3.99, 1.011);
        Assert.assertEquals(result, 5.001, 0);
    }

    @Test
    public void testSubLong() {
        long result = calculator.sub(105000, 5000);
        Assert.assertEquals(result, 100000);
    }

    @Test
    public void testSubDouble() {
        double result = calculator.sub(3.99, 3.9);
        Assert.assertEquals(result, 0.09, 0);
    }

    @Test
    public void testMultLong() {
        long result = calculator.mult(25, 4);
        Assert.assertEquals(result, 100);
    }

    @Test
    public void testMultDouble() {
        double result = calculator.mult(25.01, 4);
        Assert.assertEquals(result, 100.04, 0);
    }

    @Test(dataProvider = "divLongProvider")
    public void testDivLong(long first, long second, double expected) {
        long result = calculator.div(first, second);
        Assert.assertEquals(result, expected);
    }

    @DataProvider
    public Object[][] divLongProvider() {
        return new Object[][]{
                {6, 2, 3},
                {0, 10, 0},
                {101, 5, 20.2},
                {-250, 5, -50}
        };
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void testDivLongByZero() {
        calculator.div(100, 0);
    }

    @Test
    public void testDivDouble() {
        double result = calculator.div(100.4, 4);
        Assert.assertEquals(result, 25.1, 0);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void testDivDoubleByZero() {
        calculator.div(100.4, 0);
    }

    @Test
    public void testPow() {
        double result = calculator.pow(2, 4);
        Assert.assertEquals(result, 16);
    }

    @Test
    public void testPowDouble() {
        double result = calculator.pow(2.2, 2);
        Assert.assertEquals(result, 4.84, 0);
    }

    @Test
    public void testPowNegative() {
        double result = calculator.pow(2, -2);
        Assert.assertEquals(result, 0.25);
    }

    @Test
    public void testSqrt() {
        double result = calculator.sqrt(9);
        Assert.assertEquals(result, 3);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void testSqrtNegative() {
        calculator.sqrt(-9);
    }
}
