package com.training.yz.hw2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class MailServiceTest {
    private static WebDriver driver;
    private static WebDriverWait wait;
    private static SoftAssert softAssert;
    private static final String DRIVER_PATH = "src/test/resources/driver/chromedriver.exe";
    private static final String URL = "https://mail.ru";
    private static final String USER_LOGIN = "test_account_at@mail.ru";
    private static final String USER_PASSWORD = "superSecurePassword";
    private static final String MESSAGE = "Hello my friend";

    private final By loginLocator = By.cssSelector("[name='login']");
    private final By toInputLocator = By.cssSelector("[data-type='to'] input");
    private final By draftLetterLocator = By.cssSelector("[href*='/drafts/'][data-id]");
    private final By writeLetterLocator = By.cssSelector("[href='/compose/']");
    private final By subjectInputLocator = By.xpath("//input[@name='Subject']");
    private final By messageInputLocator = By.cssSelector("[role='textbox']");
    private final By saveButtonLocator = By.cssSelector("[data-title-shortcut='Ctrl+S']");
    private final By closeButtonLocator = By.xpath("//*[@data-promo-id='extend']/following::button");
    private final By draftsFolderLocator = By.cssSelector("[href='/drafts/']");
    private final By nextButtonLocator = By.cssSelector("[data-testid='enter-password']");
    private final By passwordInputLocator = By.cssSelector(".password-input");
    private final By loginButtonLocator = By.cssSelector("[data-testid='login-to-mail']");
    private final By sentFolderLocator = By.cssSelector("[href='/sent/']");
    private final By toInputDraftLocator = By.cssSelector("[data-type='to'] [title]");
    private final By messageInputDraftLocator = By.cssSelector("[role='textbox'] [id]>[class]>div:nth-child(1)");
    private final By sendButtonLocator = By.cssSelector("[data-title-shortcut='Ctrl+Enter']");
    private final By sentLetterLocator = By.cssSelector("[href*='/sent/'][data-id]");
    private final By logoutLocator = By.id("PH_logoutLink");
    private final By userEmailDropdownLocator = By.id("PH_user-email");

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
        softAssert = new SoftAssert();
    }

    @BeforeMethod
    public void login() {
        driver = new ChromeDriver(new ChromeOptions().addArguments("start-maximized"));
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        driver.manage().deleteAllCookies();
        driver.get(URL);
        driver.findElement(loginLocator).sendKeys(USER_LOGIN);
        driver.findElement(nextButtonLocator).click();
        driver.findElement(passwordInputLocator).sendKeys(USER_PASSWORD);
        driver.findElement(loginButtonLocator).click();
    }

    @Test
    public void testLogin() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(userEmailDropdownLocator));
        Assert.assertEquals(driver.findElement(userEmailDropdownLocator).getText(), USER_LOGIN);
    }

    @Test
    public void testSaveAsDraft() {
        wait.until(ExpectedConditions.elementToBeClickable(writeLetterLocator));
        driver.findElement(writeLetterLocator).click();

        String subject = String.valueOf(System.currentTimeMillis());

        WebElement toInput = driver.findElement(toInputLocator);
        toInput.sendKeys(USER_LOGIN);
        WebElement subjectInput = driver.findElement(subjectInputLocator);
        subjectInput.sendKeys(subject);
        WebElement messageInput = driver.findElement(messageInputLocator);
        messageInput.sendKeys(MESSAGE);
        driver.findElement(saveButtonLocator).click();
        driver.findElement(closeButtonLocator).click();

        driver.findElement(draftsFolderLocator).click();
        driver.findElements(draftLetterLocator).get(0).click();

        toInput = driver.findElement(toInputDraftLocator);
        subjectInput = driver.findElement(subjectInputLocator);
        messageInput = driver.findElement(messageInputDraftLocator);

        Assert.assertEquals(toInput.getText(), USER_LOGIN);
        Assert.assertEquals(subjectInput.getAttribute("value"), subject);
        Assert.assertEquals(messageInput.getText(), MESSAGE);
    }

    @Test
    public void testSendLetter() {
        wait.until(ExpectedConditions.elementToBeClickable(writeLetterLocator));
        driver.findElement(writeLetterLocator).click();

        String subject = String.valueOf(System.currentTimeMillis());

        driver.findElement(toInputLocator).sendKeys(USER_LOGIN);
        driver.findElement(subjectInputLocator).sendKeys(subject);
        driver.findElement(messageInputLocator).sendKeys(MESSAGE);
        driver.findElement(saveButtonLocator).click();
        driver.findElement(closeButtonLocator).click();

        driver.findElement(draftsFolderLocator).click();

        WebElement draftLetter = driver.findElements(draftLetterLocator).get(0);
        draftLetter.click();
        WebElement sendButton = driver.findElement(sendButtonLocator);
        sendButton.click();

        wait.until(ExpectedConditions.invisibilityOf(sendButton));
        driver.navigate().refresh();

        draftLetter = driver.findElements(draftLetterLocator).get(0);
        softAssert.assertFalse(draftLetter.getText().contains(subject));

        driver.findElement(sentFolderLocator).click();

        Assert.assertTrue(driver.findElements(sentLetterLocator).get(0).getText().contains(subject));
    }

    @Test
    public void testLogOut() {
        wait.until(ExpectedConditions.elementToBeClickable(logoutLocator));
        driver.findElement(logoutLocator).click();
        Assert.assertTrue(driver.findElement(loginLocator).isDisplayed());
    }

    @AfterMethod
    public void clear() {
        driver.close();
        driver.quit();
    }
}

