package com.training.yz.hw8;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class WebServiceTest {
    private static final String BASE_URI = "http://jsonplaceholder.typicode.com";
    private static final String BASE_PATH = "/albums/";

    @BeforeClass
    public void setUp() {
        RestAssured.baseURI = BASE_URI;
        RestAssured.basePath = BASE_PATH;
        RestAssured.requestSpecification = given().contentType(ContentType.JSON).accept(ContentType.JSON);
    }

    @Test
    public void testHttpStatusCode() {
        given()
                .when().get()
                .then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testHttpHeader() {
        given()
                .when().get()
                .then().contentType(equalTo("application/json; charset=utf-8"));
    }

    @Test
    public void testHttpBody() {
        given()
                .when().get()
                .then().body("size()", is(100));
    }

    @Test
    public void testGetAlbumById() {
        given()
                .when().get("{id}", 2)
                .then().body("userId", equalTo(1))
                .and().body("id", equalTo(2))
                .and().body("title", equalTo("sunt qui excepturi placeat culpa"));
    }

    @Test
    public void testCreateNewAlbum() {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("userId", 1);
        objectMap.put("title", "test album");

        int albumId = given().body(objectMap)
                .when().post()
                .then().statusCode(HttpStatus.SC_CREATED)
                .and().body("userId", notNullValue())
                .extract().path("id");

        System.out.printf("Created album with id: %s", albumId);
    }

    @Test
    public void testUpdateAlbum() {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("userId", 1);
        objectMap.put("title", "test album update");

        given().body(objectMap)
                .when().put("{id}", 4)
                .then().statusCode(HttpStatus.SC_OK)
                .and().body("id", equalTo(4));
    }

    @Test
    public void testDeleteAlbum() {
        given().when().delete("{id}", 10)
                .then().statusCode(HttpStatus.SC_OK);
    }
}
