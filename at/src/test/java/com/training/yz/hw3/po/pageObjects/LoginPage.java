package com.training.yz.hw3.po.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {
    private final By loginInput = By.cssSelector("[name='login']");
    private final By goToPasswordButton = By.cssSelector("[data-testid='enter-password']");
    private final By passwordInput = By.cssSelector(".password-input");
    private final By loginButton = By.cssSelector("[data-testid='login-to-mail']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage typeLogin(String login) {
        driver.findElement(loginInput).sendKeys(login);
        return this;
    }

    public LoginPage clickGoToPasswordButton() {
        driver.findElement(goToPasswordButton).click();
        return this;
    }

    public LoginPage typePassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
        return this;
    }

    public void clickLoginButton() {
        driver.findElement(loginButton).click();
    }

    public boolean isLoginInputVisible() {
        return driver.findElement(loginInput).isDisplayed();
    }
}
