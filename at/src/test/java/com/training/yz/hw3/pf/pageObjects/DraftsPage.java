package com.training.yz.hw3.pf.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DraftsPage extends AbstractPage {
    @FindBy(xpath = "//*[contains(@href, '/drafts/')and @data-id][1]")
    private WebElement firstDraftLetter;

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public void openDraftLetter() {
        firstDraftLetter.click();
    }

    public String getFirstLetter() {
        return firstDraftLetter.getText();
    }
}
