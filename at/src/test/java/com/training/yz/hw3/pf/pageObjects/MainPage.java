package com.training.yz.hw3.pf.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends AbstractPage {
    @FindBy(css = "[href='/compose/']")
    private WebElement writeLetterButton;
    @FindBy(css = "[href='/drafts/']")
    private WebElement draftsFolderButton;
    @FindBy(css = "[href='/sent/']")
    private WebElement sentFolderButton;
    @FindBy(id = "PH_logoutLink")
    private WebElement logoutLink;
    @FindBy(id = "PH_user-email")
    private WebElement userEmailDropdown;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public String getLogin() {
        waitForElementVisibility(userEmailDropdown);
        return userEmailDropdown.getText();
    }

    public WriteLetterPage clickWriteLetterButton() {
        waitForElementToBeClickable(writeLetterButton);
        writeLetterButton.click();
        return new WriteLetterPage(driver);
    }

    public DraftsPage openDraftsFolder() {
        draftsFolderButton.click();
        return new DraftsPage(driver);
    }

    public SentPage openSentFolder() {
        sentFolderButton.click();
        return new SentPage(driver);
    }

    public LoginPage clickLogoutLink() {
        waitForElementToBeClickable(logoutLink);
        logoutLink.click();
        return new LoginPage(driver);
    }
}
