package com.training.yz.hw3.po.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends AbstractPage {
    private final By writeLetterButton = By.cssSelector("[href='/compose/']");
    private final By draftsFolderButton = By.cssSelector("[href='/drafts/']");
    private final By sentFolderButton = By.cssSelector("[href='/sent/']");
    private final By logoutLink = By.id("PH_logoutLink");
    private final By userEmailDropdown = By.id("PH_user-email");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public String getLogin() {
        waitForElementVisibility(userEmailDropdown);
        return driver.findElement(userEmailDropdown).getText();
    }

    public WriteLetterPage clickWriteLetterButton() {
        waitForElementToBeClickable(writeLetterButton);
        driver.findElement(writeLetterButton).click();
        return new WriteLetterPage(driver);
    }

    public DraftsPage openDraftsFolder() {
        driver.findElement(draftsFolderButton).click();
        return new DraftsPage(driver);
    }

    public void openSentFolder() {
        driver.findElement(sentFolderButton).click();
    }

    public void clickLogout() {
        waitForElementToBeClickable(logoutLink);
        driver.findElement(logoutLink).click();
    }
}
