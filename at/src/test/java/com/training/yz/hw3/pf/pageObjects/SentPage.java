package com.training.yz.hw3.pf.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SentPage extends AbstractPage {
    @FindBy(xpath = "//*[contains(@href, '/sent/')and @data-id][1]")
    private WebElement firstSentLetter;

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public String getSentLetterText() {
        return firstSentLetter.getText();
    }
}
