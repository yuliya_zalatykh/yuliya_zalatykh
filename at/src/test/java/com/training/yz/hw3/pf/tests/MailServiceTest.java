package com.training.yz.hw3.pf.tests;

import com.training.yz.hw3.pf.pageObjects.DraftsPage;
import com.training.yz.hw3.pf.pageObjects.LoginPage;
import com.training.yz.hw3.pf.pageObjects.MainPage;
import com.training.yz.hw3.pf.pageObjects.SentPage;
import com.training.yz.hw3.pf.pageObjects.WriteLetterPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class MailServiceTest extends ConfigurationTest {
    private static final String MESSAGE = "Hello my friend";
    private static final String USER_LOGIN = "test_account_at@mail.ru";
    private static final String USER_PASSWORD = "superSecurePassword";

    @BeforeMethod
    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeLogin(USER_LOGIN);
        loginPage.clickNextButton();
        loginPage.typePassword(USER_PASSWORD);
        loginPage.clickLoginButton();
    }

    @Test
    public void testLogin() {
        Assert.assertEquals(new MainPage(driver).getLogin(), USER_LOGIN);
    }

    @Test
    public void testSaveAsDraft() {
        String subject = String.valueOf(System.currentTimeMillis());
        MainPage mainPage = new MainPage(driver);
        WriteLetterPage writeLetterPage = mainPage.clickWriteLetterButton();
        writeLetterPage.typeAddress(USER_LOGIN);
        writeLetterPage.typeSubject(subject);
        writeLetterPage.typeMessage(MESSAGE);
        writeLetterPage.clickSaveButton();
        writeLetterPage.clickCloseButton();
        mainPage.openDraftsFolder();
        DraftsPage draftsPage = new DraftsPage(driver);
        draftsPage.openDraftLetter();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(writeLetterPage.getAddress(), USER_LOGIN);
        softAssert.assertEquals(writeLetterPage.getSubject(), subject);
        softAssert.assertEquals(writeLetterPage.getMessage(), MESSAGE);
        softAssert.assertAll();
    }

    @Test
    public void testSendLetter() {
        String subject = String.valueOf(System.currentTimeMillis());
        MainPage mainPage = new MainPage(driver);
        WriteLetterPage writeLetterPage = mainPage.clickWriteLetterButton();
        writeLetterPage.typeAddress(USER_LOGIN);
        writeLetterPage.typeSubject(subject);
        writeLetterPage.typeMessage(MESSAGE);
        writeLetterPage.clickSaveButton();
        writeLetterPage.clickCloseButton();

        DraftsPage draftsPage = mainPage.openDraftsFolder();
        draftsPage.openDraftLetter();
        writeLetterPage.sendLetter();
        writeLetterPage.closeInfoModal();
        writeLetterPage.refreshPage();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertFalse(draftsPage.getFirstLetter().contains(subject));

        SentPage sentPage = mainPage.openSentFolder();
        softAssert.assertTrue(sentPage.getSentLetterText().contains(subject));
        softAssert.assertAll();
    }

    @Test
    public void testLogOut() {
        MainPage mainPage = new MainPage(driver);
        LoginPage loginPage = mainPage.clickLogoutLink();
        Assert.assertTrue(loginPage.isLoginInputVisible());
    }
}

