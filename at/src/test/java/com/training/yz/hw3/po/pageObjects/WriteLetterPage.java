package com.training.yz.hw3.po.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WriteLetterPage extends AbstractPage {
    private final By addressInput = By.cssSelector("[data-type='to'] input");
    private final By subjectInput = By.xpath("//input[@name='Subject']");
    private final By messageInput = By.cssSelector("[role='textbox']");
    private final By saveButton = By.cssSelector("[data-title-shortcut='Ctrl+S']");
    private final By closeButton = By.xpath("//*[@data-promo-id='extend']/following::button");
    private final By addressDraftInput = By.cssSelector("[data-type='to'] [title]");
    private final By messageDraftInput = By.cssSelector("[role='textbox'] [id]>[class]>div:nth-child(1)");
    private final By sendButton = By.cssSelector("[data-title-shortcut='Ctrl+Enter']");

    public WriteLetterPage(WebDriver driver) {
        super(driver);
    }

    public WriteLetterPage typeAddress(String address) {
        driver.findElement(addressInput).sendKeys(address);
        return this;
    }

    public WriteLetterPage typeSubject(String subject) {
        driver.findElement(subjectInput).sendKeys(subject);
        return this;
    }

    public WriteLetterPage typeMessage(String message) {
        driver.findElement(messageInput).sendKeys(message);
        return this;
    }

    public WriteLetterPage clickSaveButton() {
        driver.findElement(saveButton).click();
        return this;
    }

    public MainPage clickCloseButton() {
        driver.findElement(closeButton).click();
        return new MainPage(driver);
    }

    public String getAddress() {
        return driver.findElement(addressDraftInput).getText();
    }

    public String getSubject() {
        return driver.findElement(subjectInput).getAttribute("value");
    }

    public String getMessage() {
        return driver.findElement(messageDraftInput).getText();
    }

    public WriteLetterPage sendLetter() {
        driver.findElement(sendButton).click();
        waitForElementInvisibility(sendButton);
        return this;
    }
}
