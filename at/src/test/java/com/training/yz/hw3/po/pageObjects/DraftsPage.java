package com.training.yz.hw3.po.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends AbstractPage {
    private final By firstDraftLetter = By.xpath("//*[contains(@href, '/drafts/')and @data-id][1]");

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public WriteLetterPage openDraftLetter() {
        driver.findElement(firstDraftLetter).click();
        return new WriteLetterPage(driver);
    }

    public String getFirstLetter() {
        return driver.findElement(firstDraftLetter).getText();
    }
}
