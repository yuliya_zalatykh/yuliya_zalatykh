package com.training.yz.hw3.po.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends AbstractPage {
    private final By firstSentLetter = By.xpath("//*[contains(@href, '/sent/')and @data-id][1]");

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public String getSentLetterText() {
        return driver.findElement(firstSentLetter).getText();
    }
}
