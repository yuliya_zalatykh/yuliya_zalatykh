package com.training.yz.hw3.pf.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class ConfigurationTest {
    protected WebDriver driver;
    private static final String DRIVER_PATH = "src/test/resources/driver/chromedriver.exe";
    private static final String URL = "https://mail.ru";

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
    }

    @BeforeMethod
    public void configure() {
        driver = new ChromeDriver(new ChromeOptions().addArguments("start-maximized"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        driver.get(URL);
    }

    @AfterMethod
    public void clear() {
        driver.close();
        driver.quit();
    }
}
