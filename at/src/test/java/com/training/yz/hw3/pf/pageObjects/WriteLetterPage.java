package com.training.yz.hw3.pf.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WriteLetterPage extends AbstractPage {
    @FindBy(css = "[data-type='to'] input")
    private WebElement addressInput;
    @FindBy(xpath = "//input[@name='Subject']")
    private WebElement subjectInput;
    @FindBy(css = "[role='textbox']")
    private WebElement messageInput;
    @FindBy(css = "[data-title-shortcut='Ctrl+S']")
    private WebElement saveButton;
    @FindBy(xpath = "//*[@data-promo-id='extend']/following::button")
    private WebElement closeButton;
    @FindBy(css = "[data-type='to'] [title]")
    private WebElement addressDraftInput;
    @FindBy(css = "[role='textbox'] [id]>[class]>div:nth-child(1)")
    private WebElement messageDraftInput;
    @FindBy(css = "[data-title-shortcut='Ctrl+Enter']")
    private WebElement sendButton;
    @FindBy(css = ".button2_close")
    private WebElement closeInfoButton;

    public WriteLetterPage(WebDriver driver) {
        super(driver);
    }

    public void typeAddress(String address) {
        addressInput.sendKeys(address);
    }

    public void typeSubject(String subject) {
        subjectInput.sendKeys(subject);
    }

    public void typeMessage(String message) {
        messageInput.sendKeys(message);
    }

    public void clickSaveButton() {
        saveButton.click();
    }

    public void clickCloseButton() {
        closeButton.click();
    }

    public String getAddress() {
        return addressDraftInput.getText();
    }

    public String getSubject() {
        return subjectInput.getAttribute("value");
    }

    public String getMessage() {
        return messageDraftInput.getText();
    }

    public void sendLetter() {
        sendButton.click();
    }

    public void closeInfoModal() {
        closeInfoButton.click();
    }
}
