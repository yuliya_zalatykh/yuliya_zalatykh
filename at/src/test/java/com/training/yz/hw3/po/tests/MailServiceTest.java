package com.training.yz.hw3.po.tests;

import com.training.yz.hw3.po.pageObjects.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class MailServiceTest extends ConfigurationTest {
    private static final String MESSAGE = "Hello my friend";
    protected static final String USER_LOGIN = "test_account_at@mail.ru";
    private static final String USER_PASSWORD = "superSecurePassword";

    @BeforeMethod
    public void login() {
        new LoginPage(driver).typeLogin(USER_LOGIN).clickGoToPasswordButton().typePassword(USER_PASSWORD)
                .clickLoginButton();
    }

    @Test
    public void testLogin() {
        Assert.assertEquals(new MainPage(driver).getLogin(), USER_LOGIN);
    }

    @Test
    public void testSaveAsDraft() {
        String subject = String.valueOf(System.currentTimeMillis());
        WriteLetterPage writeLetterPage = new MainPage(driver).clickWriteLetterButton().typeAddress(USER_LOGIN)
                .typeSubject(subject).typeMessage(MESSAGE).clickSaveButton().clickCloseButton().openDraftsFolder()
                .openDraftLetter();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(writeLetterPage.getAddress(), USER_LOGIN);
        softAssert.assertEquals(writeLetterPage.getSubject(), subject);
        softAssert.assertEquals(writeLetterPage.getMessage(), MESSAGE);
        softAssert.assertAll();
    }

    @Test
    public void testSendLetter() {
        String subject = String.valueOf(System.currentTimeMillis());
        MainPage mainPage = new MainPage(driver);
        mainPage.clickWriteLetterButton().typeAddress(USER_LOGIN).typeSubject(subject).typeMessage(MESSAGE)
                .clickSaveButton().clickCloseButton().openDraftsFolder().openDraftLetter().sendLetter().refreshPage();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertFalse(new DraftsPage(driver).getFirstLetter().contains(subject));
        mainPage.openSentFolder();
        softAssert.assertTrue(new SentPage(driver).getSentLetterText().contains(subject));
        softAssert.assertAll();
    }

    @Test
    public void testLogOut() {
        new MainPage(driver).clickLogout();
        Assert.assertTrue(new LoginPage(driver).isLoginInputVisible());
    }
}
