package com.training.yz.hw3.pf.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {
    @FindBy(css = "[name='login']")
    private WebElement loginInput;
    @FindBy(css = "[data-testid='enter-password']")
    private WebElement goToPasswordButton;
    @FindBy(css = ".password-input")
    private WebElement passwordInput;
    @FindBy(css = "[data-testid='login-to-mail']")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void typeLogin(String login) {
        loginInput.sendKeys(login);
    }

    public void clickNextButton() {
        goToPasswordButton.click();
    }

    public void typePassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void clickLoginButton() {
        loginButton.click();
    }

    public boolean isLoginInputVisible() {
        return loginInput.isDisplayed();
    }
}
