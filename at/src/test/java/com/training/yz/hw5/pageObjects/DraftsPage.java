package com.training.yz.hw5.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends AbstractPage {
    private final By draftLetterItem = By.cssSelector("[href*='/drafts/'][data-id]");

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public void openDraftLetter() {
        driver.findElements(draftLetterItem).get(0).click();
    }

    public String getFirstLetterText() {
        return driver.findElements(draftLetterItem).get(0).getText();
    }
}
