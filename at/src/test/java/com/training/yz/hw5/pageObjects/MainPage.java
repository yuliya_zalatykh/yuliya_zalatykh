package com.training.yz.hw5.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends AbstractPage {
    private final By writeLetterButton = By.cssSelector("[href='/compose/']");
    private final By draftsFolderButton = By.cssSelector("[href='/drafts/']");
    private final By sentFolderButton = By.cssSelector("[href='/sent/']");
    private final By logoutLink = By.id("PH_logoutLink");
    private final By userEmailDropdown = By.id("PH_user-email");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public String getUserLogin() {
        wait.elementToBeVisible(userEmailDropdown);
        return driver.findElement(userEmailDropdown).getText();
    }

    public void clickWriteLetterButton() {
        wait.elementToBeClickable(writeLetterButton);
        driver.findElement(writeLetterButton).click();
    }

    public void openDraftsFolder() {
        driver.findElement(draftsFolderButton).click();
    }

    public void openSentFolder() {
        driver.findElement(sentFolderButton).click();
    }

    public void clickLogout() {
        wait.elementToBeClickable(logoutLink);
        driver.findElement(logoutLink).click();
    }
}
