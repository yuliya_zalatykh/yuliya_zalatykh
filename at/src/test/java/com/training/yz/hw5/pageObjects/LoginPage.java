package com.training.yz.hw5.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {
    private final By loginInput = By.cssSelector("[name='login']");
    private final By passwordButton = By.cssSelector("[data-testid='enter-password']");
    private final By passwordInput = By.cssSelector(".password-input");
    private final By loginButton = By.cssSelector("[data-testid='login-to-mail']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void typeLogin(String login) {
        driver.findElement(loginInput).sendKeys(login);
    }

    public void clickNextButton() {
        driver.findElement(passwordButton).click();
    }

    public void typePassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(loginButton).click();
    }

    public boolean isLoginButtonDisplayed() {
        return driver.findElement(loginInput).isDisplayed();
    }
}
