package com.training.yz.hw5.services;

import com.training.yz.hw5.bussinesObjects.User;
import com.training.yz.hw5.pageObjects.LoginPage;
import com.training.yz.hw5.pageObjects.MainPage;
import org.openqa.selenium.WebDriver;

public class LoginService {
    private WebDriver driver;
    private LoginPage loginPage;

    public LoginService(WebDriver driver) {
        this.driver = driver;
        loginPage = new LoginPage(driver);
    }

    public void signIn(User user) {
        loginPage.typeLogin(user.getLogin());
        loginPage.clickNextButton();
        loginPage.typePassword(user.getPassword());
        loginPage.clickLoginButton();
    }

    public void logOut() {
        new MainPage(driver).clickLogout();
    }

    public boolean isLoggedOut() {
        return loginPage.isLoginButtonDisplayed();
    }
}
