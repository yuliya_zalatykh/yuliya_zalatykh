package com.training.yz.hw5.tests;

import com.training.yz.hw5.bussinesObjects.User;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class ConfigurationTest {
    protected static WebDriver driver;
    protected User user = new User("test_account_at@mail.ru", "superSecurePassword");

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass(alwaysRun = true)
    public void reset() {
        driver.quit();
        driver = null;
    }
}
