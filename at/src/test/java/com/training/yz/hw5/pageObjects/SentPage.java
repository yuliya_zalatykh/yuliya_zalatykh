package com.training.yz.hw5.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends AbstractPage {
    private final By sentLetterItem = By.cssSelector("[href*='/sent/'][data-id]");

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public String getSentLetterText() {
        return driver.findElements(sentLetterItem).get(0).getText();
    }
}
