package com.training.yz.hw5.pageObjects;

import com.training.yz.hw5.utils.Wait;
import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {
    protected final WebDriver driver;
    protected final Wait wait;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver);
    }

    public void refreshPage() {
        driver.navigate().refresh();
    }
}
