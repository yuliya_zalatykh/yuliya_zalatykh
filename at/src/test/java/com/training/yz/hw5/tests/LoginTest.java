package com.training.yz.hw5.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;

public class LoginTest {
    private static final String URL = "https://mail.ru";
    private static final String USER_LOGIN = "test_account_at@mail.ru";
    private static final String USER_PASSWORD = "superSecurePassword";
    private WebDriver driver;

    @Test
    public void testLogin() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        setWebDriver(driver);
        open(URL);
        $("[name='login']").sendKeys(USER_LOGIN);
        $("[data-testid='enter-password']").click();
        $(".password-input").sendKeys(USER_PASSWORD);
        $("[data-testid='login-to-mail']").click();
        By userEmailDropdown = By.id("PH_user-email");
        $(userEmailDropdown).shouldBe(visible);
        Assert.assertEquals($(userEmailDropdown).getText(), USER_LOGIN);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        driver.close();
        driver = null;
    }
}
