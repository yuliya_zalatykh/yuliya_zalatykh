package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.WebDriver;

public abstract class Page {
    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }
}
