package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends Page {
    private final By dragElement = By.id("draggable");
    private final By dropElement = By.id("droppable");

    public DroppablePage(WebDriver driver) {
        super(driver);
        driver.switchTo().frame(driver.findElement(By.className("demo-frame")));
    }

    public void dragAndDrop() {
        new Actions(driver).dragAndDrop(driver.findElement(dragElement), driver.findElement(dropElement))
                .build().perform();
    }

    public String getDragAndDropResult() {
        return driver.findElement(dropElement).getText();
    }
}
