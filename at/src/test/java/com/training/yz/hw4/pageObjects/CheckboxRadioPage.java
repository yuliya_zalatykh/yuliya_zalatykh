package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class CheckboxRadioPage extends Page {
    private final By radioElementLocator = By.cssSelector("label[for=radio-1]");
    private final By checkBoxElementLocator = By.cssSelector("label[for=checkbox-1]");

    public CheckboxRadioPage(WebDriver driver) {
        super(driver);
        driver.switchTo().frame(driver.findElement(By.className("demo-frame")));
    }

    public CheckboxRadioPage selectRadioButton() {
        new Actions(driver).click(driver.findElement(radioElementLocator)).build().perform();
        return this;
    }

    public boolean isRadioButtonChecked() {
        return driver.findElement(radioElementLocator).getAttribute("class").contains("checked");
    }

    public CheckboxRadioPage selectCheckBox() {
        new Actions(driver).click(driver.findElement(checkBoxElementLocator)).build().perform();
        return this;
    }

    public boolean isCheckBoxChecked() {
        return driver.findElement(checkBoxElementLocator).getAttribute("class").contains("checked");
    }
}
