package com.training.yz.hw4.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class ConfigurationTest {
    protected WebDriver driver;
    private static final String TEST_URL = "http://jqueryui.com";
    private static final String DRIVER_PATH = "src/test/resources/driver/chromedriver.exe";

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
    }

    @BeforeMethod
    public void open() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(TEST_URL);
    }

    @AfterMethod
    public void clear() {
        driver.close();
    }

    @AfterClass
    public void reset() {
        driver.quit();
    }
}
