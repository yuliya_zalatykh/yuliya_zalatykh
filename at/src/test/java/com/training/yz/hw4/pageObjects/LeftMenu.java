package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LeftMenu extends Page {
    public LeftMenu(WebDriver driver) {
        super(driver);
    }

    public void navigateTo(String pageName) {
        driver.findElement(By.cssSelector(String.format("#sidebar a[href*=%s]", pageName.toLowerCase()))).click();
    }
}
