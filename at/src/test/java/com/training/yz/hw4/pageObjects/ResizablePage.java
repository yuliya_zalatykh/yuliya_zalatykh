package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ResizablePage extends Page {
    private final By resizableElementLocator = By.cssSelector("#resizable div[class*=ui-icon-gripsmall-diagonal-se]");
    private String basicProperties;

    public ResizablePage(WebDriver driver) {
        super(driver);
        driver.switchTo().frame(driver.findElement(By.className("demo-frame")));
    }

    public String resizeElement() {
        WebElement resizeElement = driver.findElement(resizableElementLocator);
        basicProperties = resizeElement.getAttribute("style");
        new Actions(driver).clickAndHold(resizeElement).moveByOffset(30, 30).release().build().perform();
        return basicProperties;
    }

    public String getResizableElementProperties() {
        return driver.findElement(resizableElementLocator).getAttribute("title");
    }
}
