package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TooltipsPage extends Page {
    private final By paragraph = By.cssSelector("p");
    private final By tooltip = By.cssSelector(".ui-tooltip");
    private final By linkedLabel1 = By.xpath("//a[@href='#']");
    private final By linkedLabel2 = By.xpath("//p/a[contains(@href,'themeroller')]");
    private final By textbox = By.id("age");

    public TooltipsPage(WebDriver driver) {
        super(driver);
        driver.switchTo().frame(driver.findElement(By.className("demo-frame")));
    }

    public String getTooltipTextFromLinkedLabel1() {
        return getTooltipText(linkedLabel1);
    }

    public String getTooltipTextFromLinkedLabel2() {
        return getTooltipText(linkedLabel2);
    }

    public String getTooltipTextFromTextbox() {
        return getTooltipText(textbox);
    }

    private String getTooltipText(By locator) {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(locator)).build().perform();
        String text = driver.findElement(tooltip).getText();
        action.moveToElement(driver.findElement(paragraph)).build().perform();
        new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOfElementLocated(tooltip));
        return text;
    }
}
