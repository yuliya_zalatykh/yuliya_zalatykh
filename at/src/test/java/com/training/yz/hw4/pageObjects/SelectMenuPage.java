package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class SelectMenuPage extends Page {
    private final By speedSelectMenu = By.id("speed-button");
    private final By numberSelectMenu = By.id("number-button");
    private final By speedValue = By.id("ui-id-4");
    private final By numberValue = By.id("ui-id-8");

    public SelectMenuPage(WebDriver driver) {
        super(driver);
        driver.switchTo().frame(driver.findElement(By.className("demo-frame")));
    }

    public void selectSpeed() {
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(speedSelectMenu)).build().perform();
        actions.click(driver.findElement(speedValue)).build().perform();
    }

    public void selectNumber() {
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(numberSelectMenu)).build().perform();
        actions.click(driver.findElement(numberValue)).build().perform();
    }

    public String getSpeedSelectMenuAttribute() {
        return driver.findElement(speedSelectMenu).getAttribute("aria-labelledby");
    }

    public String getNumberSelectMenuAttribute() {
        return driver.findElement(numberSelectMenu).getAttribute("aria-labelledby");
    }
}
