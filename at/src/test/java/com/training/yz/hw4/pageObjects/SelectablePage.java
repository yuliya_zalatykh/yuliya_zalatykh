package com.training.yz.hw4.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class SelectablePage extends Page {
    private final By selectableItems = By.cssSelector("[id=selectable]>li");
    private List<WebElement> listOfItems;

    public SelectablePage(WebDriver driver) {
        super(driver);
        driver.switchTo().frame(driver.findElement(By.className("demo-frame")));
    }

    public void selectItems() {
        listOfItems = driver.findElements(selectableItems);
        new Actions(driver).keyDown(Keys.CONTROL)
                .click(listOfItems.get(2)).click(listOfItems.get(4)).click(listOfItems.get(6))
                .keyUp(Keys.CONTROL).build().perform();
    }

    public boolean isSetOfItemsSelected() {
        return listOfItems.get(2).getAttribute("class").contains("ui-selected")
                && listOfItems.get(4).getAttribute("class").contains("ui-selected")
                && listOfItems.get(6).getAttribute("class").contains("ui-selected");
    }
}
