package com.training.yz.hw4.tests;

import com.training.yz.hw4.pageObjects.CheckboxRadioPage;
import com.training.yz.hw4.pageObjects.DroppablePage;
import com.training.yz.hw4.pageObjects.LeftMenu;
import com.training.yz.hw4.pageObjects.ResizablePage;
import com.training.yz.hw4.pageObjects.SelectMenuPage;
import com.training.yz.hw4.pageObjects.SelectablePage;
import com.training.yz.hw4.pageObjects.TooltipsPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AppTest extends ConfigurationTest {

    @Test
    public void dragNDropTest() {
        new LeftMenu(driver).navigateTo("Droppable");
        DroppablePage droppablePage = new DroppablePage(driver);
        droppablePage.dragAndDrop();
        Assert.assertEquals(droppablePage.getDragAndDropResult(), "Dropped!");
    }

    @Test
    public void checkBoxRadioTest() {
        new LeftMenu(driver).navigateTo("Checkboxradio");
        CheckboxRadioPage checkboxRadioPage = new CheckboxRadioPage(driver).selectRadioButton().selectCheckBox();
        Assert.assertTrue(checkboxRadioPage.isRadioButtonChecked());
        Assert.assertTrue(checkboxRadioPage.isCheckBoxChecked());
    }

    @Test
    public void selectMenuTest() {
        new LeftMenu(driver).navigateTo("Selectmenu");
        SelectMenuPage selectmenuPage = new SelectMenuPage(driver);
        selectmenuPage.selectSpeed();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(selectmenuPage.getSpeedSelectMenuAttribute(), "ui-id-4");
        selectmenuPage.selectNumber();
        softAssert.assertEquals(selectmenuPage.getNumberSelectMenuAttribute(), "ui-id-8");
        softAssert.assertAll();
    }

    @Test
    public void tooltipsTest() {
        new LeftMenu(driver).navigateTo("Tooltip");
        TooltipsPage tooltipsPage = new TooltipsPage(driver);
        Assert.assertEquals(tooltipsPage.getTooltipTextFromLinkedLabel1(), "That's what this widget is");
        Assert.assertEquals(tooltipsPage.getTooltipTextFromLinkedLabel2(), "ThemeRoller: jQuery UI's theme builder application");
        Assert.assertEquals(tooltipsPage.getTooltipTextFromTextbox(), "We ask for your age only for statistical purposes.");
    }

    @Test
    public void resizeTest() {
        new LeftMenu(driver).navigateTo("Resizable");
        ResizablePage resizablePage = new ResizablePage(driver);
        String properties = resizablePage.resizeElement();
        Assert.assertNotEquals(resizablePage.getResizableElementProperties(), properties);
    }

    @Test
    public void selectTest() {
        new LeftMenu(driver).navigateTo("Selectable");
        SelectablePage selectablePage = new SelectablePage(driver);
        selectablePage.selectItems();
        Assert.assertTrue(selectablePage.isSetOfItemsSelected());
    }
}
