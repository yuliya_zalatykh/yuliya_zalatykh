package com.training.yz.hw9.businessObjects;

import java.util.Objects;

public class Item {
    private String name;
    private String price;
    private String seller;

    public Item(String name, String price, String seller) {
        this.name = name;
        this.price = price;
        this.seller = seller;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return Objects.equals(name, item.name) && Objects.equals(price, item.price) && Objects.equals(seller, item.seller);
    }
}
