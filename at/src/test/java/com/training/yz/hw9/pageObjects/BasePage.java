package com.training.yz.hw9.pageObjects;

import com.training.yz.hw9.browser.Browser;
import com.training.yz.hw9.utils.AdvancedFeatures;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.log4testng.Logger;

public class BasePage {
    private final By searchInput = By.id("gh-ac");
    private final By searchButton = By.id("gh-btn");
    private final By changeLangBtn = By.id("gh-eb-Geo-a-default");
    private final By usLangBtn = By.id("gh-eb-Geo-a-en");
    private final By changeLanguageButton = By.cssSelector("html[lang]");
    private static final String BASE_URL = "https://www.ebay.com/";
    private final Logger logger = Logger.getLogger(BasePage.class);

    public void open() {
        logger.debug("Open base page");
        Browser.getInstance().get(BASE_URL);
        String lang = Browser.getInstance().findElement(changeLanguageButton).getAttribute("lang");
        if (!lang.equals("en")) {
            Browser.getInstance().findElement(changeLangBtn).click();
            new Actions(Browser.getInstance())
                    .moveToElement(Browser.getInstance().findElement(usLangBtn)).click().build().perform();
        }
    }

    public void enterTextToSearchInput(String text) {
        logger.debug(String.format("Enter %s to search input", text));
        WebElement searchInputElement = Browser.getInstance().findElement(searchInput);
        searchInputElement.sendKeys(text);
        AdvancedFeatures.highlightElement(searchInputElement);
    }

    public void clickSearchButton() {
        Browser.getInstance().findElement(searchButton).click();
    }
}
