package com.training.yz.hw9.pageObjects;

import com.training.yz.hw9.browser.Browser;
import org.openqa.selenium.By;
import org.testng.log4testng.Logger;

public class CartPage extends BasePage {
    private final By itemNameLabel = By.cssSelector("[data-test-id=cart-item-link] span.BOLD");
    private final By itemPriceLabel = By.cssSelector(".item-price");
    private final By itemSellerLabel = By.xpath("//*[@class='black-link']/descendant::a");
    private final By ctaButton = By.cssSelector(".call-to-action, .start-shop");
    private final Logger logger = Logger.getLogger(CartPage.class);

    public String getItemName() {
        logger.debug("Get item name");
        return Browser.getInstance().findElement(itemNameLabel).getText();
    }

    public String getItemPrice() {
        logger.debug("Get item price");
        return Browser.getInstance().findElement(itemPriceLabel).getText();
    }

    public String getItemSeller() {
        logger.debug("Get item seller");
        return Browser.getInstance().findElement(itemSellerLabel).getText();
    }

    public boolean checkCardVisibility() {
        logger.debug("Check card visibility");
        return Browser.getInstance().findElement(ctaButton).isDisplayed();
    }
}
