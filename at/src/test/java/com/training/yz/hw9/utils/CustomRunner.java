package com.training.yz.hw9.utils;

import io.cucumber.junit.Cucumber;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

public class CustomRunner extends Cucumber {

    public CustomRunner(Class clazz) throws InitializationError {
        super(clazz);
    }

    @Override
    public void run(RunNotifier notifier) {
        notifier.addListener(new CustomListener());
        super.run(notifier);
    }
}
