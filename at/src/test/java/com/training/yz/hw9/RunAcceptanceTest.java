package com.training.yz.hw9;

import com.training.yz.hw9.browser.Browser;
import com.training.yz.hw9.utils.CustomRunner;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.testng.log4testng.Logger;

@RunWith(CustomRunner.class)
@CucumberOptions(
        features = "src/test/resources/com.training.yz.hw9/features",
        plugin = {
                "pretty",
                "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }, tags = "@smoke"
)
public class RunAcceptanceTest {

    @AfterClass()
    public static void closeDriver() {
        Logger.getLogger(RunAcceptanceTest.class).debug("Close browser");
        Browser.close();
    }
}
