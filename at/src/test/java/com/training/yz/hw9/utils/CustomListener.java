package com.training.yz.hw9.utils;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.testng.log4testng.Logger;

public class CustomListener extends RunListener {
    private final Logger logger = Logger.getLogger(CustomListener.class);

    @Override
    public void testStarted(Description description) {
        logger.debug("Started: " + description.getDisplayName());
    }

    @Override
    public void testFinished(Description description) {
        logger.debug("Finished: " + description.getDisplayName());
        AdvancedFeatures.takeScreenshot();
    }

    @Override
    public void testFailure(Failure failure) {
        logger.error("Failed: " + failure.getTestHeader());
    }
}
