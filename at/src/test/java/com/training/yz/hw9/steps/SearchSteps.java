package com.training.yz.hw9.steps;

import com.training.yz.hw9.pageObjects.BasePage;
import com.training.yz.hw9.pageObjects.SearchResultPage;
import com.training.yz.hw9.utils.AdvancedFeatures;
import io.cucumber.java.AfterStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.log4testng.Logger;

import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SearchSteps {
    private final BasePage basePage = new BasePage();
    private final Logger logger = Logger.getLogger(SearchSteps.class);

    @Given("User opens Ebay")
    public void userOpensEbay() {
        basePage.open();
        logger.info("Open Ebay");
    }

    @When("User searches {string}")
    public void userSearches(String text) {
        logger.info("Search " + text);
        basePage.enterTextToSearchInput(text);
        basePage.clickSearchButton();
    }

    @Then("First search result should contain {string}")
    public void firstSearchResultShouldContain(String text) {
        logger.debug("Assert search result");
        assertThat(new SearchResultPage().getFirstItemName().toLowerCase(),
                containsString(text.toLowerCase(Locale.ROOT)));
    }

    @AfterStep
    public void takeScreenshot() {
        AdvancedFeatures.takeScreenshot();
    }
}
