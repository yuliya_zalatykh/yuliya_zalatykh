package com.training.yz.hw9.steps;

import com.training.yz.hw9.businessObjects.Item;
import com.training.yz.hw9.pageObjects.CartPage;
import com.training.yz.hw9.pageObjects.ItemPage;
import com.training.yz.hw9.pageObjects.SearchResultPage;
import com.training.yz.hw9.utils.AdvancedFeatures;
import io.cucumber.java.AfterStep;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.log4testng.Logger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AddToCartSteps {
    private Item item;
    private ItemPage itemPage = new ItemPage();
    private final Logger logger = Logger.getLogger(AddToCartSteps.class);

    @When("User clicks first item name")
    public void userClicksFirstItemName() {
        new SearchResultPage().clickFirstItemNameLink();
        logger.info("Click first item name");
        item = new Item(itemPage.getItemTitle(), itemPage.getItemPrice(), itemPage.getItemSeller());
    }

    @When("User clicks add to cart button")
    public void userClicksAddToCartButton() {
        itemPage.clickAddToCartButton();
        logger.info("Click add to cart button");
    }

    @Then("User is on Cart page")
    public void userIsOnCartPage() {
        assertThat(new CartPage().checkCardVisibility(), is(true));
        logger.info("Cart page is opened");
    }

    @Then("Item should be visible")
    public void itemShouldBeVisible() {
        CartPage cartPage = new CartPage();
        logger.info("Assert cart item");
        Item cartItem = new Item(cartPage.getItemName(), cartPage.getItemPrice(), cartPage.getItemSeller());
        assertThat(cartItem.equals(item), is(true));
    }

    @AfterStep
    public void takeScreenshot() {
        AdvancedFeatures.takeScreenshot();
    }
}
