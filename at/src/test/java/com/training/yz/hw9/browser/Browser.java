package com.training.yz.hw9.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.log4testng.Logger;

import java.util.concurrent.TimeUnit;

public class Browser {
    private static WebDriver driver;
    private static Logger logger = Logger.getLogger(Browser.class);

    public static WebDriver getInstance() {
        if (driver == null) {
            init();
        }
        return driver;
    }

    private static void init() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(new ChromeOptions().addArguments("--incognito  --start-maximized"));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public static void close() {
        try {
            driver.quit();
        } catch (Exception exception) {
            logger.error(String.format("Close error \n %s", exception.getMessage()));
        } finally {
            driver = null;
        }
    }
}
