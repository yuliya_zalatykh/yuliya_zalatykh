package com.training.yz.hw9.pageObjects;

import com.training.yz.hw9.browser.Browser;
import org.openqa.selenium.By;
import org.testng.log4testng.Logger;

public class SearchResultPage extends BasePage {
    private final By itemNameLink = By.cssSelector(".s-item__link");
    private final Logger logger = Logger.getLogger(SearchResultPage.class);

    public void clickFirstItemNameLink() {
        logger.debug("Click first item name link");
        Browser.getInstance().findElement(itemNameLink).click();
    }

    public String getFirstItemName() {
        logger.debug("Get first item name");
        return Browser.getInstance().findElement(itemNameLink).getText();
    }
}
