package com.training.yz.hw9.utils;

import com.training.yz.hw9.browser.Browser;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AdvancedFeatures {

    public static void highlightElement(WebElement element) {
        if (Browser.getInstance() instanceof JavascriptExecutor) {
            ((JavascriptExecutor) Browser.getInstance()).executeScript("arguments[0].style.border='5px solid red'", element);
        }
    }

    public static void takeScreenshot() {
        File scFile = ((TakesScreenshot) Browser.getInstance()).getScreenshotAs(OutputType.FILE);
        DateFormat sdf = new SimpleDateFormat("MM.dd.yyyy HH-mm-ss  ");
        String formattedDate = sdf.format(new Date());
        try {
            FileUtils.copyFile(scFile, new File("target/screenshots/" + formattedDate + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
