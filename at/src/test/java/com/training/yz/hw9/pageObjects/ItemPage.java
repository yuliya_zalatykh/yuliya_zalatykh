package com.training.yz.hw9.pageObjects;

import com.training.yz.hw9.browser.Browser;
import org.openqa.selenium.By;
import org.testng.log4testng.Logger;

public class ItemPage extends BasePage {
    private final By itemTitleLink = By.id("itemTitle");
    private final By itemPrice = By.id("prcIsum");
    private final By itemSeller = By.cssSelector(".mbg-nw");
    private final By addToCartButton = By.id("isCartBtn_btn");
    private final Logger logger = Logger.getLogger(ItemPage.class);

    public String getItemTitle() {
        logger.debug("Get item title");
        return Browser.getInstance().findElement(itemTitleLink).getText();
    }

    public String getItemPrice() {
        logger.debug("get item price");
        return Browser.getInstance().findElement(itemPrice).getText();
    }

    public String getItemSeller() {
        logger.debug("Get item seller");
        return Browser.getInstance().findElement(itemSeller).getText();
    }

    public void clickAddToCartButton() {
        logger.debug("Click add to card button");
        Browser.getInstance().findElement(addToCartButton).click();
    }
}
