package com.training.yz.hw7;

import com.training.yz.hw7.browser.Browser;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/com.training.yz.hw7/features",
        plugin = {
                "pretty",
                "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }, tags = "@smoke"
)
public class RunAcceptanceTest {

    @AfterClass()
    public static void closeDriver() {
        Browser.close();
    }
}
