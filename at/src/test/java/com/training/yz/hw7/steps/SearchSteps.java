package com.training.yz.hw7.steps;

import com.training.yz.hw7.pageObjects.BasePage;
import com.training.yz.hw7.pageObjects.SearchResultPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SearchSteps {
    private final BasePage basePage = new BasePage();

    @Given("User opens Ebay")
    public void userOpensEbay() {
        basePage.open();
    }

    @When("User searches {string}")
    public void userSearches(String text) {
        basePage.enterTextToSearchInput(text);
        basePage.clickSearchButton();
    }

    @Then("First search result should contain {string}")
    public void firstSearchResultShouldContain(String text) {
        assertThat(new SearchResultPage().getFirstItemName().toLowerCase(),
                containsString(text.toLowerCase(Locale.ROOT)));
    }
}
