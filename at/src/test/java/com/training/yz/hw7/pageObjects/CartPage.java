package com.training.yz.hw7.pageObjects;

import com.training.yz.hw7.browser.Browser;
import org.openqa.selenium.By;

public class CartPage extends BasePage {
    private final By itemNameLabel = By.cssSelector("[data-test-id=cart-item-link] span.BOLD");
    private final By itemPriceLabel = By.cssSelector(".item-price");
    private final By itemSellerLabel = By.xpath("//*[@class='black-link']/descendant::a");
    private final By ctaButton = By.cssSelector(".call-to-action, .start-shop");

    public String getItemName() {
        return Browser.getInstance().findElement(itemNameLabel).getText();
    }

    public String getItemPrice() {
        return Browser.getInstance().findElement(itemPriceLabel).getText();
    }

    public String getItemSeller() {
        return Browser.getInstance().findElement(itemSellerLabel).getText();
    }

    public boolean checkCardVisibility() {
        return Browser.getInstance().findElement(ctaButton).isDisplayed();
    }
}
