package com.training.yz.hw7.pageObjects;

import com.training.yz.hw7.browser.Browser;
import org.openqa.selenium.By;

public class SearchResultPage extends BasePage {
    private final By itemNameLink = By.cssSelector(".s-item__link");

    public void clickFirstItemNameLink() {
        Browser.getInstance().findElement(itemNameLink).click();
    }

    public String getFirstItemName() {
        return Browser.getInstance().findElement(itemNameLink).getText();
    }
}
