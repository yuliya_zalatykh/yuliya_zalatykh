package com.training.yz.hw7.steps;

import com.training.yz.hw7.businessObjects.Item;
import com.training.yz.hw7.pageObjects.CartPage;
import com.training.yz.hw7.pageObjects.ItemPage;
import com.training.yz.hw7.pageObjects.SearchResultPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AddToCartSteps {
    private Item item;
    private ItemPage itemPage = new ItemPage();

    @When("User clicks first item name")
    public void userClicksFirstItemName() {
        new SearchResultPage().clickFirstItemNameLink();
        item = new Item(itemPage.getItemTitle(), itemPage.getItemPrice(), itemPage.getItemSeller());
    }

    @When("User clicks add to cart button")
    public void userClicksAddToCartButton() {
        itemPage.clickAddToCartButton();
    }

    @Then("User is on Cart page")
    public void userIsOnCartPage() {
        assertThat(new CartPage().checkCardVisibility(), is(true));
    }

    @Then("Item should be visible")
    public void itemShouldBeVisible() {
        CartPage cartPage = new CartPage();
        Item cartItem = new Item(cartPage.getItemName(), cartPage.getItemPrice(), cartPage.getItemSeller());
        assertThat(cartItem.equals(item), is(true));
    }
}
