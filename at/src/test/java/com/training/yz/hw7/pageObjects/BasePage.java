package com.training.yz.hw7.pageObjects;

import com.training.yz.hw7.browser.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class BasePage {
    private final By searchInput = By.id("gh-ac");
    private final By searchButton = By.id("gh-btn");
    private final By changeLangBtn = By.id("gh-eb-Geo-a-default");
    private final By usLangBtn = By.id("gh-eb-Geo-a-en");
    private final By changeLanguageButton = By.cssSelector("html[lang]");
    private static final String BASE_URL = "https://www.ebay.com/";

    public void open() {
        Browser.getInstance().get(BASE_URL);
        String lang = Browser.getInstance().findElement(changeLanguageButton).getAttribute("lang");
        if (!lang.equals("en")) {
            Browser.getInstance().findElement(changeLangBtn).click();
            new Actions(Browser.getInstance())
                    .moveToElement(Browser.getInstance().findElement(usLangBtn)).click().build().perform();
        }
    }

    public void enterTextToSearchInput(String text) {
        Browser.getInstance().findElement(searchInput).sendKeys(text);
    }

    public void clickSearchButton() {
        Browser.getInstance().findElement(searchButton).click();
    }
}
