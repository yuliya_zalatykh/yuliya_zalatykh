package com.training.yz.hw7.pageObjects;

import com.training.yz.hw7.browser.Browser;
import org.openqa.selenium.By;

public class ItemPage extends BasePage {
    private final By itemTitleLink = By.id("itemTitle");
    private final By itemPrice = By.id("prcIsum");
    private final By itemSeller = By.cssSelector(".mbg-nw");
    private final By addToCartButton = By.id("isCartBtn_btn");

    public String getItemTitle() {
        return Browser.getInstance().findElement(itemTitleLink).getText();
    }

    public String getItemPrice() {
        return Browser.getInstance().findElement(itemPrice).getText();
    }

    public String getItemSeller() {
        return Browser.getInstance().findElement(itemSeller).getText();
    }

    public void clickAddToCartButton() {
        Browser.getInstance().findElement(addToCartButton).click();
    }
}
