@smoke
Feature: Search

  Scenario: Search product "cucumber"
    Given User opens Ebay
    When User searches "cucumber"
    Then First search result should contain "cucumber"

  Scenario Outline: Search product "<product>"
    Given User opens Ebay
    When User searches "<product>"
    Then First search result should contain "<product>"

    Examples:
      | product |
      | tomato  |
      | apple   |
