@smoke
Feature: Cart

  Scenario: Add product to cart
    Given User opens Ebay
    And User searches "cucumber"
    When User clicks first item name
    And User clicks add to cart button
    Then User is on Cart page
    And Item should be visible
